import cv2
from TextDetection import *
import re
from GoogleSearch import *

def processText():

    cam = cv2.VideoCapture(1)

    ret_val, img = cam.read()

    (xmin, xmax, ymin, ymax) = (40, 600, 220, 300)
    cv2.imwrite("question.jpg", img)
    prompt = detect_text('question.jpg')
    print("Prompt: " + str(prompt))
    scrub_prompt = re.sub(r'(.)*\s[H|a|O|Q]*(.)*\s([0-9])\s', '', prompt)
    print(scrub_prompt)
    print("")

    question = re.sub('[?](.)*', '', scrub_prompt) + "?"
    print("Question: " + question)

    answers = re.sub('(.)*[?]', '', scrub_prompt).strip()
    ans_list = answers.split()
    print("A: " + ans_list[0])
    print("B: " + ans_list[1])
    print("C: " + ans_list[2])

    answer_dict = {}
    total_scores = 0
    for ans in ans_list:

        score = getAnswer(ans, question)
        answer_dict[ans] = score
        total_scores += score


    for key in answer_dict:
        answer_dict[key] = answer_dict[key]/total_scores

    print(answer_dict)


    #
    # (xmin, xmax, ymin, ymax) = (40, 600, 176, 220)
    # cv2.imwrite("choice1.jpg", img[ymin:ymax, xmin:xmax, :])
    # question = detect_text('choice1.jpg')
    # print("Choice 1: " + str(question))
    #
    # (xmin, xmax, ymin, ymax) = (40, 600, 126, 176)
    # cv2.imwrite("choice2.jpg", img[ymin:ymax, xmin:xmax, :])
    # question = detect_text('choice2.jpg')
    # print("Choice 2 " + str(question))
    #
    # (xmin, xmax, ymin, ymax) = (40, 600, 80, 126)
    # cv2.imwrite("choice3.jpg", img[ymin:ymax, xmin:xmax, :])
    # question = detect_text('choice3.jpg')
    # print("Choice 3: " + str(question))

    # if mirror:
    #     img = cv2.flip(img, 1)

processText()



