import cv2
import time

def show_webcam(mirror=True):
    cam = cv2.VideoCapture(1)
    #print("Cam Delay")
    time.sleep(2)
    while True:
        #print("Cam Read")
        ret_val, img = cam.read()

        cv2.imwrite("test.jpg", img)

        # if mirror:
        #     img = cv2.flip(img, 1)
        cv2.imshow('my webcam', img)
        if cv2.waitKey(1) == 27:
            break  # esc to quit
        time.sleep(1/20.0)
    cv2.destroyAllWindows()

def main():
    show_webcam(mirror=False)

if __name__ == '__main__':
    main()