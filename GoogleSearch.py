import urllib.request
import re

def search(term):
    url = "https://en.wikipedia.org/wiki/" + term
    print(url)
    f = urllib.request.urlopen(url)
    return str(f.read())

def getAnswer(alleged_answer, question):
    html = search(alleged_answer)
    question = re.sub('[?|\"]', '', question)

    question_terms = question.split(' ')
    print(question_terms)
    score = 0
    for word in question_terms:
        if word.lower() not in ['is', 'of', 'these', 'the', 'which', 'what', 'why', 'who', 'how', 'a', 'its',
                                'gets', 'for', 'from']:
            if html.lower().find(' ' + word.lower()+ ' ') != -1:
                print(word + " was found in wikipedia")
                score += 1
    return score