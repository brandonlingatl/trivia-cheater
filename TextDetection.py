import io

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types


def detect_text_bytes(content):
    """Detects text in the file."""
    client = vision.ImageAnnotatorClient()

    image = types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    return texts

    # print('Texts:')
    # output = ""
    # for text in texts:
    #     print('\n"{}"'.format(text.description))
    #     output += '\n"{}"'.format(text.description)
    #
    #     vertices = (['({},{})'.format(vertex.x, vertex.y)
    #                  for vertex in text.bounding_poly.vertices])
    #
    #     print('bounds: {}'.format(','.join(vertices)))
    #     output += 'bounds: {}'.format(','.join(vertices))

    # f = open('text_out.txt', 'w')
    # f.write(output)
    # f.close()


def detect_text(path):
    """Detects text in the file."""
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    #print(len(texts))
    if len(texts) > 0:
        return '\n"{}"'.format(texts[0].description).replace("\n", " ").replace("\"", "")
    else:
        return ""
    # print('Texts:')
    # output = ""
    # for text in texts:
    #     print('\n"{}"'.format(text.description))
    #     output += '\n"{}"'.format(text.description)
    #
    #     vertices = (['({},{})'.format(vertex.x, vertex.y)
    #                  for vertex in text.bounding_poly.vertices])
    #
    #     print('bounds: {}'.format(','.join(vertices)))
    #     output += 'bounds: {}'.format(','.join(vertices))
    #
    # f = open('text_out.txt', 'w')
    # f.write(output)
    # f.close()

# detect_text(r"C:\Users\Meta\PycharmProjects\TriviaCheater\resources\IMG_2130.PNG")